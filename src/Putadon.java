import java.util.Scanner;

/*
 * CSI Florida
 * Reto Putadon
 * 
 * El siguiente c�digo busca una letra dentro de una frase
 * y devuelve d�nde se ha encontrado y ctas veces
 * 
 * Hay dos errores
 * 
 */
public class Putadon {
	
	public static void main(String[] args) {
		//Variables inicialización
		String frase="";
		char letra=0;
		int numPos=0,numRepeticiones=0;
		//Pedimos datos
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Introduce tu frase");
		frase = sc.nextLine();
		
		System.out.println("Introduce la letra a buscar");
		letra = sc.nextLine().charAt(0); //charAt(0) devuelve la primera letra
		
		//Imprime y busca datos 
		imprimeLetras(frase,letra,numRepeticiones);
	}
	public static void imprimeLetras (String frase,char letra,int numRepeticiones) {
		//Buscamos las letras
		for(int i=0;i<frase.length();i++){
			if(letra==frase.charAt(i)){
				System.out.println("Letra "+letra+" encontrada en la posicion "+i);
				numRepeticiones += 1;
			}
		}
		System.out.println("Total repeticiones "+numRepeticiones);
	}
}
